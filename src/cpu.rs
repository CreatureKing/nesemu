use utils;

use byteorder::{LittleEndian, ByteOrder};

use memory::Memory;

#[derive(Debug, Copy, Clone)]
pub enum AddressMode {
    Acc,    // Accumulator	 	        OPC A	 	    operand is AC
    Abs,    // absolute        	 	    OPC $HHLL	 	operand is address $HHLL
    AbsX,  // absolute, X-indexed	 	OPC $HHLL,X	 	operand is address incremented by X with carry
    AbsY,  // absolute, Y-indexed	 	OPC $HHLL,Y	 	operand is address incremented by Y with carry
    Imm,    // immediate	        	OPC #$BB	 	operand is byte (BB)
    Impl,   // implied	 	            OPC	 	        operand implied
    Ind,    // indirect	        	    kOPC ($HHLL)	operand is effective address; effective address is value of address
    IndX,  // X-indexed, indirect	 	OPC ($BB,X)	 	operand is effective zeropage address; effective address is byte (BB) incremented by X without carry
    IndY,  // indirect, Y-indexed	 	OPC ($LL),Y	 	operand is effective address incremented by Y with carry; effective address is word at zeropage address
    Rel,    // relative	        	    OPC $BB	     	branch target is PC + offset (BB), bit 7 signifies negative offset
    Zpg,    // zeropage	 	            OPC $LL	 	    operand is of address; address hibyte = zero ($00xx)
    ZpgX,  // zeropage, X-indexed	 	OPC $LL,X	 	operand is address incremented by X; address hibyte = zero ($00xx); no page transition
    ZpgY   // zeropage, Y-indexed	 	OPC $LL,Y	 	operand is address incremented by Y; address hibyte = zero ($00xx); no page transition
}

macro_rules! operands {
    ($s:ident, Acc) => {
        {
            (None, None)
        }
    };
    ($s:ident, Abs) => {
        {
            let operand1 = $s.pc_increment();
            let operand2 = $s.pc_increment();
            (Some(operand1), Some(operand2))
        }
    };
    ($s:ident, AbsX) => {
        {
            let operand1 = $s.pc_increment();
            let operand2 = $s.pc_increment();
            (Some(operand1), Some(operand2))
        }
    };
    ($s:ident, AbsY) => {
        {
            let operand1 = $s.pc_increment();
            let operand2 = $s.pc_increment();
            (Some(operand1), Some(operand2))
        }
    };
    ($s:ident, Imm) => {
        {
            let operand1 = $s.pc_increment();
            (Some(operand1), None)
        }
    };
    ($s:ident, Impl) => {
        {
            (None, None)
        }
    };
    ($s:ident, Ind) => {
        {
            let operand1 = $s.pc_increment();
            let operand2 = $s.pc_increment();
            (Some(operand1), Some(operand2))
        }
    };
    ($s:ident, IndX) => {
        {
            let operand1 = $s.pc_increment();
            (Some(operand1), None)
        }
    };
    ($s:ident, IndY) => {
        {
            let operand1 = $s.pc_increment();
            (Some(operand1), None)
        }
    };
    ($s:ident, Rel) => {
        {
            let operand1 = $s.pc_increment();
            (Some(operand1), None)
        }
    };
    ($s:ident, Zpg) => {
        {
            let operand1 = $s.pc_increment();
            (Some(operand1), None)
        }
    };
    ($s:ident, ZpgX) => {
        {
            let operand1 = $s.pc_increment();
            (Some(operand1), None)
        }
    };
    ($s:ident, ZpgY) => {
        {
            let operand1 = $s.pc_increment();
            (Some(operand1), None)
        }
    };
}

macro_rules! call {
    ($s:ident, $func:ident, $cycles:expr, $mode:ident) => {
        {
            let opcode = $s.read($s.pc - 1);
            let (operand1, operand2): (Option<u8>, Option<u8>) = operands!($s, $mode);
            debug!("{}", utils::format_op_string(&$s, opcode, stringify!($func), &$mode, &operand1, &operand2));
            $s.$func($cycles, $mode, operand1, operand2)
        }
    }
}

pub struct CPU {
    pub a: u8,   // Accumulator
    pub x: u8,   // Index X
    pub y: u8,   // Index Y
    pub pc: u16, // Program Counter
    pub s: u8,   // Stack Pointer
    pub p: u8,    // Status Register
    pub memory: Memory
}

use self::AddressMode::*;

impl CPU {
    pub fn new(memory: Memory) -> CPU {
        let mut cpu = CPU {
            a: 0,
            x: 0,
            y: 0,
            pc: 0,
            s: 0,
            p: 0,
            memory
        };
        cpu.reset();
        cpu
    }

    fn reset(&mut self) {
        self.memory.cpu_reset();
        self.a = 0;
        self.x = 0;
        self.y = 0;
        self.pc = LittleEndian::read_u16(&[self.read(0xFFFC), self.read(0xFFFD)]);
        self.s = 0xFF;
        self.p = 0b00100100;
    }

    pub fn status_negative(&self) -> bool {
        self.p & 0b10000000 > 0
    }

    fn set_status_negative(&mut self, status: bool) {
        self.p = utils::set_bit(self.p, 7, status);
    }

    pub fn status_overflow(&self) -> bool {
        self.p & 0b01000000 > 0
    }

    fn set_status_overflow(&mut self, status: bool) {
        self.p = utils::set_bit(self.p, 6, status);
    }

    pub fn status_break(&self) -> bool {
        self.p & 0b00010000 > 0
    }

    fn set_status_break(&mut self, status: bool) {
        self.p = utils::set_bit(self.p, 4, status);
    }

    pub fn status_decimal(&self) -> bool {
        self.p & 0b00001000 > 0
    }

    fn set_status_decimal(&mut self, status: bool) {
        self.p = utils::set_bit(self.p, 3, status);
    }

    pub fn status_interrupt(&self) -> bool {
        self.p & 0b00000100 > 0
    }

    fn set_status_interrupt(&mut self, status: bool) {
        self.p = utils::set_bit(self.p, 2, status);
    }

    pub fn status_zero(&self) -> bool {
        self.p & 0b00000010 > 0
    }

    fn set_status_zero(&mut self, status: bool) {
        self.p = utils::set_bit(self.p, 1, status);
    }

    pub fn status_carry(&self) -> bool {
        self.p & 0b00000001 > 0
    }

    fn set_status_carry(&mut self, status: bool) {
        self.p = utils::set_bit(self.p, 0, status);
    }

    pub fn read(&self, address: u16) -> u8 {
        self.memory.cpu_read(address)
    }

    fn write(&mut self, address: u16, data: u8) {
        self.memory.cpu_write(address, data);
    }

    fn pc_increment(&mut self) -> u8 {
        let res = self.read(self.pc);
        self.pc += 1;
        res
    }

    fn push_stack(&mut self, data: u8) {
        let sp = 0x0100 | (self.s as u16);
        self.write(sp, data);
        self.s -= 1;
    }

    fn pop_stack(&mut self) -> u8 {
        self.s += 1;
        let sp = 0x0100 | (self.s as u16);
        self.read(sp as u16)
    }

    fn address(&self, mode: AddressMode, operand1: Option<u8>, operand2: Option<u8>) -> u16 {
        match mode {
            Abs => {
                let low = operand1.unwrap() as u16;
                let high = operand2.unwrap() as u16;
                (high << 8) | low
            },
            AbsX => {
                let low = operand1.unwrap() as u16;
                let high = operand2.unwrap() as u16;
                ((high << 8) | low).wrapping_add(self.x as u16)
            },
            AbsY => {
                let low = operand1.unwrap() as u16;
                let high = operand2.unwrap() as u16;
                ((high << 8) | low).wrapping_add(self.y as u16)
            },
            Ind => {
                let op1 = operand1.unwrap() as u16;
                let op2 = operand2.unwrap() as u16;
                let ll = (op2 << 8) | op1;
                let hl = ll.wrapping_add(1);
                let low = self.read(ll) as u16;
                let high = self.read(hl) as u16;
                (high << 8) | low
            },
            IndX => {
                let ll = operand1.unwrap().wrapping_add(self.x);
                let hl = ll.wrapping_add(1);
                let low = self.read(ll as u16) as u16;
                let high = self.read(hl as u16) as u16;
                (high << 8) | low
            },
            IndY => {
                let ll = operand1.unwrap();
                let hl = ll.wrapping_add(1);
                let low = self.read(ll as u16) as u16;
                let high = self.read(hl as u16) as u16;
                ((high << 8) | low).wrapping_add(self.y as u16)
            },
            Rel => {
                let op1 = operand1.unwrap() as u16;
                if op1 & 0x80 > 0 {
                    self.pc - (0x0100 - op1)
                } else {
                    self.pc + op1
                }
            },
            Zpg => {
                operand1.unwrap() as u16
            },
            ZpgX => {
                let op1 = operand1.unwrap();
                op1.wrapping_add(self.x) as u16
            },
            ZpgY => {
                let op1 = operand1.unwrap();
                op1.wrapping_add(self.y) as u16
            },
            _ => panic!(format!("Invalid Address Mode {:?}", mode))
        }
    }

    fn value(&self, mode: AddressMode, operand1: Option<u8>, operand2: Option<u8>) -> u8 {
        match mode {
            Acc => self.a,
            Imm => operand1.unwrap(),
            _ => {
                let address = self.address(mode, operand1, operand2);
                self.read(address)
            }
        }
    }

    fn ADC(&mut self, base_cycles: i32, mode: AddressMode, 
           operand1: Option<u8>, operand2: Option<u8>) -> Result<i32, String> {
        let val = self.value(mode, operand1, operand2);
        let sum = (self.a as u16) + (val as u16) + (self.status_carry() as u16);

        self.set_status_carry(sum > 0xFF);
        let overflow = ((sum ^ val as u16) & (sum ^ self.a as u16) & 0x80) != 0;
        self.set_status_overflow(overflow); self.set_status_zero(sum & 0xFF == 0);
        self.set_status_negative(sum & 0x80 > 0);

        self.a = (sum & 0xFF) as u8;

        Ok(base_cycles)
    }

    fn AND(&mut self, base_cycles: i32, mode: AddressMode, 
           operand1: Option<u8>, operand2: Option<u8>) -> Result<i32, String> {
        let res = self.a & self.value(mode, operand1, operand2);
        self.set_status_zero(res == 0);
        self.set_status_negative(res >= 0x80);
        self.a = res;
    
        Ok(base_cycles)
    }

    fn ASL(&mut self, base_cycles: i32, mode: AddressMode, 
           operand1: Option<u8>, operand2: Option<u8>) -> Result<i32, String> {
        let mut val = self.value(mode, operand1, operand2);

        self.set_status_carry(val & 0x80 > 0);
        val <<= 1;

        self.set_status_negative(val >= 0x80);
        self.set_status_zero(val == 0);
        match mode {
            Acc => self.a = val,
            _ => {
                let address = self.address(mode, operand1, operand2);
                self.write(address, val);
            }
        }

        Ok(base_cycles)
    }

    fn BCC(&mut self, base_cycles: i32, mode: AddressMode, 
           operand1: Option<u8>, operand2: Option<u8>) -> Result<i32, String> {
        let address = self.address(mode, operand1, operand2);
        if !self.status_carry() {
            self.pc = address;
        }
        Ok(base_cycles)
    }

    fn BCS(&mut self, base_cycles: i32, mode: AddressMode, 
           operand1: Option<u8>, operand2: Option<u8>) -> Result<i32, String> {
        let address = self.address(mode, operand1, operand2);
        if self.status_carry() {
            self.pc = address;
        }
        Ok(base_cycles)
    }

    fn BEQ(&mut self, base_cycles: i32, mode: AddressMode, 
           operand1: Option<u8>, operand2: Option<u8>) -> Result<i32, String> {
        let address = self.address(mode, operand1, operand2);
        if self.status_zero() {
            self.pc = address;
        }
        Ok(base_cycles)
    }

    fn BIT(&mut self, base_cycles: i32, mode: AddressMode, 
           operand1: Option<u8>, operand2: Option<u8>) -> Result<i32, String> {
        let val = self.value(mode, operand1, operand2);
        let res = val & self.a;
        self.set_status_negative(val >= 0x80);
        self.set_status_overflow((val & 0x40) > 0);
        self.set_status_zero(res == 0);
        Ok(base_cycles)
    }

    fn BMI(&mut self, base_cycles: i32, mode: AddressMode, 
           operand1: Option<u8>, operand2: Option<u8>) -> Result<i32, String> {
        let address = self.address(mode, operand1, operand2);
        if self.status_negative() {
            self.pc = address;
        }
        Ok(base_cycles)
    }

    fn BNE(&mut self, base_cycles: i32, mode: AddressMode, 
           operand1: Option<u8>, operand2: Option<u8>) -> Result<i32, String> {
        let address = self.address(mode, operand1, operand2);
        if !self.status_zero() {
            self.pc = address;
        }
        Ok(base_cycles)
    }

    fn BPL(&mut self, base_cycles: i32, mode: AddressMode, 
           operand1: Option<u8>, operand2: Option<u8>) -> Result<i32, String> {
        let address = self.address(mode, operand1, operand2);
        if !self.status_negative() {
            self.pc = address;
        }
        Ok(base_cycles)
    }

    fn BRK(&mut self, base_cycles: i32, mode: AddressMode, 
           operand1: Option<u8>, operand2: Option<u8>) -> Result<i32, String> {
        let newpc = self.pc + 1;
        self.push_stack((newpc >> 8) as u8);
        self.push_stack(newpc as u8);

        self.set_status_break(true);
        let status = self.p;
        self.push_stack(status);

        self.set_status_interrupt(true);
        self.pc = self.read(0xFFFE) as u16 | ((self.read(0xFFFF) as u16) << 8);
        Ok(base_cycles)
    }

    fn BVC(&mut self, base_cycles: i32, mode: AddressMode, 
           operand1: Option<u8>, operand2: Option<u8>) -> Result<i32, String> {
        let address = self.address(mode, operand1, operand2);
        if !self.status_overflow() {
            self.pc = address;
        }
        Ok(base_cycles)
    }

    fn BVS(&mut self, base_cycles: i32, mode: AddressMode, 
           operand1: Option<u8>, operand2: Option<u8>) -> Result<i32, String> {
        let address = self.address(mode, operand1, operand2);
        if self.status_overflow() {
            self.pc = address;
        }
        Ok(base_cycles)
    }

    fn CLC(&mut self, base_cycles: i32, mode: AddressMode, 
           operand1: Option<u8>, operand2: Option<u8>) -> Result<i32, String> {
        self.set_status_carry(false);
        Ok(base_cycles)
    }

    fn CLD(&mut self, base_cycles: i32, mode: AddressMode, 
           operand1: Option<u8>, operand2: Option<u8>) -> Result<i32, String> {
        self.set_status_decimal(false);
        Ok(base_cycles)
    }

    fn CLI(&mut self, base_cycles: i32, mode: AddressMode, 
           operand1: Option<u8>, operand2: Option<u8>) -> Result<i32, String> {
        self.set_status_interrupt(false);
        Ok(base_cycles)
    }

    fn CLV(&mut self, base_cycles: i32, mode: AddressMode, 
           operand1: Option<u8>, operand2: Option<u8>) -> Result<i32, String> {
        self.set_status_overflow(false);
        Ok(base_cycles)
    }

    fn CMP(&mut self, base_cycles: i32, mode: AddressMode, 
           operand1: Option<u8>, operand2: Option<u8>) -> Result<i32, String> {
        let val = self.value(mode, operand1, operand2);
        let reg = self.a;
        let (res, overflow) = reg.overflowing_sub(val);
        self.set_status_carry(!overflow);
        self.set_status_zero(res == 0);
        self.set_status_negative(res >= 0x80);

        Ok(base_cycles)
    }

    fn CPX(&mut self, base_cycles: i32, mode: AddressMode, 
           operand1: Option<u8>, operand2: Option<u8>) -> Result<i32, String> {
        let val = self.value(mode, operand1, operand2);
        let reg = self.x;
        let (res, overflow) = reg.overflowing_sub(val);
        self.set_status_carry(!overflow);
        self.set_status_zero(res == 0);
        self.set_status_negative(res >= 0x80);

        Ok(base_cycles)
    }

    fn CPY(&mut self, base_cycles: i32, mode: AddressMode, 
           operand1: Option<u8>, operand2: Option<u8>) -> Result<i32, String> {
        let val = self.value(mode, operand1, operand2);
        let reg = self.y;
        let (res, overflow) = reg.overflowing_sub(val);
        self.set_status_carry(!overflow);
        self.set_status_zero(res == 0);
        self.set_status_negative(res >= 0x80);

        Ok(base_cycles)
    }
    
    fn DEC(&mut self, base_cycles: i32, mode: AddressMode, 
           operand1: Option<u8>, operand2: Option<u8>) -> Result<i32, String> {
        let address = self.address(mode, operand1, operand2);
        let res = self.read(address).wrapping_sub(1);
        self.set_status_zero(res == 0);
        self.set_status_negative(res >= 0x80);
        self.write(address, res);
        
        Ok(base_cycles)
    }

    fn DEX(&mut self, base_cycles: i32, mode: AddressMode, 
           operand1: Option<u8>, operand2: Option<u8>) -> Result<i32, String> {
        let res = self.x.wrapping_sub(1);
        self.set_status_zero(res == 0);
        self.set_status_negative(res >= 0x80);
        self.x = res;

        Ok(base_cycles)
    }


    fn DEY(&mut self, base_cycles: i32, mode: AddressMode, 
           operand1: Option<u8>, operand2: Option<u8>) -> Result<i32, String> {

        let res = self.y.wrapping_sub(1);
        self.set_status_zero(res == 0);
        self.set_status_negative(res >= 0x80);
        self.y = res;

        Ok(base_cycles)
    }

    fn EOR(&mut self, base_cycles: i32, mode: AddressMode, 
           operand1: Option<u8>, operand2: Option<u8>) -> Result<i32, String> {
        let val = self.value(mode, operand1, operand2);
        let res = val ^ self.a;
        self.set_status_zero(res == 0);
        self.set_status_negative(res >= 0x80);
        self.a = res;

        Ok(base_cycles)
    }
    
    fn INC(&mut self, base_cycles: i32, mode: AddressMode, 
           operand1: Option<u8>, operand2: Option<u8>) -> Result<i32, String> {
        let address = self.address(mode, operand1, operand2);
        let res = self.read(address).wrapping_add(1);
        self.set_status_zero(res == 0);
        self.set_status_negative(res >= 0x80);
        self.write(address, res);

        Ok(base_cycles)
    }

    fn INX(&mut self, base_cycles: i32, mode: AddressMode, 
           operand1: Option<u8>, operand2: Option<u8>) -> Result<i32, String> {
        let res = self.x.wrapping_add(1);
        self.set_status_zero(res == 0);
        self.set_status_negative(res >= 0x80);
        self.x = res;

        Ok(base_cycles)
    }

    fn INY(&mut self, base_cycles: i32, mode: AddressMode, 
           operand1: Option<u8>, operand2: Option<u8>) -> Result<i32, String> {
        let res = self.y.wrapping_add(1);
        self.set_status_zero(res == 0);
        self.set_status_negative(res >= 0x80);
        self.y = res;

        Ok(base_cycles)
    }

    fn JMP(&mut self, base_cycles: i32, mode: AddressMode, 
           operand1: Option<u8>, operand2: Option<u8>) -> Result<i32, String> {
        // Hardware bug causes an Indirect JMP to $xxFF to ignore 
        let address = match mode {
            Ind => {
                let op1 = operand1.unwrap() as u16;
                let op2 = operand2.unwrap() as u16;
                let ll = (op2 << 8) | op1;
                let hl = if op1 == 0xFF {
                    ll.wrapping_add(1).wrapping_sub(0x0100)
                } else {
                    ll.wrapping_add(1)
                };
                let low = self.read(ll) as u16;
                let high = self.read(hl) as u16;
                (high << 8) | low
            },
            _ => self.address(mode, operand1, operand2)
        };

        self.pc = address;
        Ok(base_cycles)
    }

    fn JSR(&mut self, base_cycles: i32, mode: AddressMode, 
           operand1: Option<u8>, operand2: Option<u8>) -> Result<i32, String> {
        let address = self.address(mode, operand1, operand2);
        let ret_addr = self.pc - 1;
        self.push_stack((ret_addr >> 8) as u8);
        self.push_stack(ret_addr as u8);
        self.pc = address;
        Ok(base_cycles)
    }

    fn LDA(&mut self, base_cycles: i32, mode: AddressMode, 
           operand1: Option<u8>, operand2: Option<u8>) -> Result<i32, String> {
        let res = self.value(mode, operand1, operand2);
        self.set_status_zero(res == 0);
        self.set_status_negative(res >= 0x80);
        self.a = res;

        Ok(base_cycles)
    }

    fn LDX(&mut self, base_cycles: i32, mode: AddressMode, 
           operand1: Option<u8>, operand2: Option<u8>) -> Result<i32, String> {
        let res = self.value(mode, operand1, operand2);
        self.set_status_zero(res == 0);
        self.set_status_negative(res >= 0x80);
        self.x = res;

        Ok(base_cycles)
    }

    fn LDY(&mut self, base_cycles: i32, mode: AddressMode, 
           operand1: Option<u8>, operand2: Option<u8>) -> Result<i32, String> {
        let res = self.value(mode, operand1, operand2);
        self.set_status_zero(res == 0);
        self.set_status_negative(res >= 0x80);
        self.y = res;

        Ok(base_cycles)
    }

    fn LSR(&mut self, base_cycles: i32, mode: AddressMode, 
           operand1: Option<u8>, operand2: Option<u8>) -> Result<i32, String> {
        let mut val = self.value(mode, operand1, operand2);

        self.set_status_carry(val & 0x01 > 0);
        val >>= 1;

        self.set_status_negative(false);
        self.set_status_zero(val == 0);
        match mode {
            Acc => self.a = val,
            _ => {
                let address = self.address(mode, operand1, operand2);
                self.write(address, val);
            }
        }

        Ok(base_cycles)
    }

    fn NOP(&mut self, base_cycles: i32, mode: AddressMode, 
           operand1: Option<u8>, operand2: Option<u8>) -> Result<i32, String> {
        Ok(base_cycles)
    }

    fn ORA(&mut self, base_cycles: i32, mode: AddressMode, 
           operand1: Option<u8>, operand2: Option<u8>) -> Result<i32, String> {
        let val = self.value(mode, operand1, operand2);
        let res = val | self.a;
        self.set_status_zero(res == 0);
        self.set_status_negative(res >= 0x80);
        self.a = res;

        Ok(base_cycles)
    }

    fn PHA(&mut self, base_cycles: i32, mode: AddressMode, 
           operand1: Option<u8>, operand2: Option<u8>) -> Result<i32, String> {
        let res = self.a;
        self.push_stack(res);
        Ok(base_cycles)
    }

    fn PHP(&mut self, base_cycles: i32, mode: AddressMode, 
           operand1: Option<u8>, operand2: Option<u8>) -> Result<i32, String> {
        let res = self.p;
        self.push_stack(res);
        Ok(base_cycles)
    }

    fn PLA(&mut self, base_cycles: i32, mode: AddressMode, 
           operand1: Option<u8>, operand2: Option<u8>) -> Result<i32, String> {
        let res = self.pop_stack();
        self.set_status_zero(res == 0);
        self.set_status_negative(res >= 0x80);
        self.a = res;

        Ok(base_cycles)
    }

    fn PLP(&mut self, base_cycles: i32, mode: AddressMode, 
           operand1: Option<u8>, operand2: Option<u8>) -> Result<i32, String> {
        let mut res = self.pop_stack();
        res = utils::set_bit(res, 5, true);
        res = utils::set_bit(res, 4, false);
        self.p = res;
        Ok(base_cycles)
    }

    fn ROL(&mut self, base_cycles: i32, mode: AddressMode, 
           operand1: Option<u8>, operand2: Option<u8>) -> Result<i32, String> {
        let mut val = self.value(mode, operand1, operand2);

        let carry_bit = self.status_carry() as u8;
        self.set_status_carry(val & 0x80 > 0);
        val = (val << 1) | carry_bit;

        self.set_status_negative(val >= 0x80);
        self.set_status_zero(val == 0);
        match mode {
            Acc => self.a = val,
            _ => {
                let address = self.address(mode, operand1, operand2);
                self.write(address, val);
            }
        }

        Ok(base_cycles)
    }

    fn ROR(&mut self, base_cycles: i32, mode: AddressMode, 
           operand1: Option<u8>, operand2: Option<u8>) -> Result<i32, String> {
        let mut val = self.value(mode, operand1, operand2);

        let carry_bit = if self.status_carry() { 0x80 } else { 0x00 };
        self.set_status_carry(val & 0x01 > 0);
        val = (val >> 1) | carry_bit;

        self.set_status_negative(val >= 0x80);
        self.set_status_zero(val == 0);
        match mode {
            Acc => self.a = val,
            _ => {
                let address = self.address(mode, operand1, operand2);
                self.write(address, val);
            }
        }
        Ok(base_cycles)
    }

    fn RTI(&mut self, base_cycles: i32, mode: AddressMode, 
           operand1: Option<u8>, operand2: Option<u8>) -> Result<i32, String> {
        let mut status = self.pop_stack();
        let pc_low = self.pop_stack();
        let pc_high = self.pop_stack();

        status = utils::set_bit(status, 5, true);
        status = utils::set_bit(status, 4, false);
        self.p = status;
        self.pc = (pc_high as u16) << 8 | (pc_low as u16);
        Ok(base_cycles)
    }

    fn RTS(&mut self, base_cycles: i32, mode: AddressMode, 
           operand1: Option<u8>, operand2: Option<u8>) -> Result<i32, String> {
        let pc_low = self.pop_stack();
        let pc_high = self.pop_stack();
        self.pc = ((pc_high as u16) << 8 | (pc_low as u16)) + 1;
        Ok(base_cycles)
    }

    fn SBC(&mut self, base_cycles: i32, mode: AddressMode, 
           operand1: Option<u8>, operand2: Option<u8>) -> Result<i32, String> {
        let val = !self.value(mode, operand1, operand2);
        let sum = (self.a as u16) + (val as u16) + (self.status_carry() as u16);

        self.set_status_carry(sum > 0xFF);
        let overflow = ((sum ^ val as u16) & (sum ^ self.a as u16) & 0x80) != 0;
        self.set_status_overflow(overflow); self.set_status_zero(sum & 0xFF == 0);
        self.set_status_negative(sum & 0x80 > 0);

        self.a = (sum & 0xFF) as u8;

        Ok(base_cycles)
    }

    fn SEC(&mut self, base_cycles: i32, mode: AddressMode, 
           operand1: Option<u8>, operand2: Option<u8>) -> Result<i32, String> {
        self.set_status_carry(true);
        Ok(base_cycles)
    }

    fn SED(&mut self, base_cycles: i32, mode: AddressMode, 
           operand1: Option<u8>, operand2: Option<u8>) -> Result<i32, String> {
        self.set_status_decimal(true);
        Ok(base_cycles)
    }

    fn SEI(&mut self, base_cycles: i32, mode: AddressMode, 
           operand1: Option<u8>, operand2: Option<u8>) -> Result<i32, String> {
        self.set_status_interrupt(true);
        Ok(base_cycles)
    }

    fn STA(&mut self, base_cycles: i32, mode: AddressMode, 
           operand1: Option<u8>, operand2: Option<u8>) -> Result<i32, String> {
        let address = self.address(mode, operand1, operand2);
        let res = self.a;
        self.write(address, res);

        Ok(base_cycles)
    }

    fn STX(&mut self, base_cycles: i32, mode: AddressMode, 
           operand1: Option<u8>, operand2: Option<u8>) -> Result<i32, String> {
        let address = self.address(mode, operand1, operand2);
        let res = self.x;
        self.write(address, res);

        Ok(base_cycles)
    }

    fn STY(&mut self, base_cycles: i32, mode: AddressMode, 
           operand1: Option<u8>, operand2: Option<u8>) -> Result<i32, String> {
        let address = self.address(mode, operand1, operand2);
        let res = self.y;
        self.write(address, res);

        Ok(base_cycles)
    }

    fn TAX(&mut self, base_cycles: i32, mode: AddressMode, 
           operand1: Option<u8>, operand2: Option<u8>) -> Result<i32, String> {
        let res = self.a;
        self.set_status_zero(res == 0);
        self.set_status_negative(res >= 0x80);
        self.x = res;

        Ok(base_cycles)
    }

    fn TAY(&mut self, base_cycles: i32, mode: AddressMode, 
           operand1: Option<u8>, operand2: Option<u8>) -> Result<i32, String> {
        let res = self.a;
        self.set_status_zero(res == 0);
        self.set_status_negative(res >= 0x80);
        self.y = res;

        Ok(base_cycles)
    }

    fn TSX(&mut self, base_cycles: i32, mode: AddressMode, 
           operand1: Option<u8>, operand2: Option<u8>) -> Result<i32, String> {
        let res = self.s;
        self.set_status_zero(res == 0);
        self.set_status_negative(res >= 0x80);
        self.x = res;

        Ok(base_cycles)
    }

    fn TXA(&mut self, base_cycles: i32, mode: AddressMode, 
           operand1: Option<u8>, operand2: Option<u8>) -> Result<i32, String> {
        let res = self.x;
        self.set_status_zero(res == 0);
        self.set_status_negative(res >= 0x80);
        self.a = res;

        Ok(base_cycles)
    }

    fn TXS(&mut self, base_cycles: i32, mode: AddressMode, 
           operand1: Option<u8>, operand2: Option<u8>) -> Result<i32, String> {
        let res = self.x;
        self.s = res;

        Ok(base_cycles)
    }

    fn TYA(&mut self, base_cycles: i32, mode: AddressMode, 
           operand1: Option<u8>, operand2: Option<u8>) -> Result<i32, String> {
        let res = self.y;
        self.set_status_zero(res == 0);
        self.set_status_negative(res >= 0x80);
        self.a = res;

        Ok(base_cycles)
    }

    pub fn run(&mut self, cycles: u32) -> u32 {
        let mut cur_cycles: u32 = 0;
        while cur_cycles < cycles {
            let instr = self.pc_increment();
            // Process instruction
            cur_cycles += match instr {
                0x69 => call!(self, ADC, 2, Imm).unwrap(),
                0x65 => call!(self, ADC, 3, Zpg).unwrap(),
                0x75 => call!(self, ADC, 4, ZpgX).unwrap(),
                0x6D => call!(self, ADC, 4, Abs).unwrap(),
                0x7D => call!(self, ADC, 4, AbsX).unwrap(),
                0x79 => call!(self, ADC, 4, AbsY).unwrap(),
                0x61 => call!(self, ADC, 6, IndX).unwrap(),
                0x71 => call!(self, ADC, 5, IndY).unwrap(),

                0x29 => call!(self, AND, 2, Imm).unwrap(),
                0x25 => call!(self, AND, 3, Zpg).unwrap(),
                0x35 => call!(self, AND, 4, ZpgX).unwrap(),
                0x2D => call!(self, AND, 4, Abs).unwrap(),
                0x3D => call!(self, AND, 4, AbsX).unwrap(),
                0x39 => call!(self, AND, 4, AbsY).unwrap(),
                0x21 => call!(self, AND, 6, IndX).unwrap(),
                0x31 => call!(self, AND, 5, IndY).unwrap(),

                0x0A => call!(self, ASL, 2, Acc).unwrap(),
                0x06 => call!(self, ASL, 5, Zpg).unwrap(),
                0x16 => call!(self, ASL, 6, ZpgX).unwrap(),
                0x0E => call!(self, ASL, 6, Abs).unwrap(),
                0x1E => call!(self, ASL, 7, AbsX).unwrap(),

                0x90 => call!(self, BCC, 2, Rel).unwrap(),

                0xB0 => call!(self, BCS, 2, Rel).unwrap(),

                0xF0 => call!(self, BEQ, 2, Rel).unwrap(),

                0x24 => call!(self, BIT, 3, Zpg).unwrap(),
                0x2C => call!(self, BIT, 4, Abs).unwrap(),

                0x30 => call!(self, BMI, 2, Rel).unwrap(),

                0xD0 => call!(self, BNE, 2, Rel).unwrap(),

                0x10 => call!(self, BPL, 2, Rel).unwrap(),

                0x00 => call!(self, BRK, 7, Impl).unwrap(),

                0x50 => call!(self, BVC, 2, Rel).unwrap(),

                0x70 => call!(self, BVS, 2, Rel).unwrap(),

                0x18 => call!(self, CLC, 2, Impl).unwrap(),

                0xD8 => call!(self, CLD, 2, Impl).unwrap(),

                0x58 => call!(self, CLI, 2, Impl).unwrap(),

                0xB8 => call!(self, CLV, 2, Impl).unwrap(),

                0xC9 => call!(self, CMP, 2, Imm).unwrap(),
                0xC5 => call!(self, CMP, 3, Zpg).unwrap(),
                0xD5 => call!(self, CMP, 4, ZpgX).unwrap(),
                0xCD => call!(self, CMP, 4, Abs).unwrap(),
                0xDD => call!(self, CMP, 4, AbsX).unwrap(),
                0xD9 => call!(self, CMP, 4, AbsY).unwrap(),
                0xC1 => call!(self, CMP, 6, IndX).unwrap(),
                0xD1 => call!(self, CMP, 5, IndY).unwrap(),

                0xE0 => call!(self, CPX, 2, Imm).unwrap(),
                0xE4 => call!(self, CPX, 3, Zpg).unwrap(),
                0xEC => call!(self, CPX, 4, Abs).unwrap(),

                0xC0 => call!(self, CPY, 2, Imm).unwrap(),
                0xC4 => call!(self, CPY, 3, Zpg).unwrap(),
                0xCC => call!(self, CPY, 4, Abs).unwrap(),

                0xC6 => call!(self, DEC, 5, Zpg).unwrap(),
                0xD6 => call!(self, DEC, 6, ZpgX).unwrap(),
                0xCE => call!(self, DEC, 6, Abs).unwrap(),
                0xDE => call!(self, DEC, 7, AbsX).unwrap(),

                0xCA => call!(self, DEX, 2, Impl).unwrap(),

                0x88 => call!(self, DEY, 2, Impl).unwrap(),

                0x49 => call!(self, EOR, 2, Imm).unwrap(),
                0x45 => call!(self, EOR, 3, Zpg).unwrap(),
                0x55 => call!(self, EOR, 4, ZpgX).unwrap(),
                0x4D => call!(self, EOR, 4, Abs).unwrap(),
                0x5D => call!(self, EOR, 4, AbsX).unwrap(),
                0x59 => call!(self, EOR, 4, AbsY).unwrap(),
                0x41 => call!(self, EOR, 6, IndX).unwrap(),
                0x51 => call!(self, EOR, 5, IndY).unwrap(),

                0xE6 => call!(self, INC, 5, Zpg).unwrap(),
                0xF6 => call!(self, INC, 6, ZpgX).unwrap(),
                0xEE => call!(self, INC, 6, Abs).unwrap(),
                0xFE => call!(self, INC, 7, AbsX).unwrap(),

                0xE8 => call!(self, INX, 2, Impl).unwrap(),

                0xC8 => call!(self, INY, 2, Impl).unwrap(),

                0x4C => call!(self, JMP, 3, Abs).unwrap(),
                0x6C => call!(self, JMP, 5, Ind).unwrap(),
                0x20 => call!(self, JSR, 6, Abs).unwrap(),

                0xA9 => call!(self, LDA, 2, Imm).unwrap(),
                0xA5 => call!(self, LDA, 3, Zpg).unwrap(),
                0xB5 => call!(self, LDA, 4, ZpgX).unwrap(),
                0xAD => call!(self, LDA, 4, Abs).unwrap(),
                0xBD => call!(self, LDA, 4, AbsX).unwrap(),
                0xB9 => call!(self, LDA, 4, AbsY).unwrap(),
                0xA1 => call!(self, LDA, 6, IndX).unwrap(),
                0xB1 => call!(self, LDA, 5, IndY).unwrap(),

                0xA2 => call!(self, LDX, 2, Imm).unwrap(),
                0xA6 => call!(self, LDX, 3, Zpg).unwrap(),
                0xB6 => call!(self, LDX, 4, ZpgY).unwrap(),
                0xAE => call!(self, LDX, 4, Abs).unwrap(),
                0xBE => call!(self, LDX, 4, AbsY).unwrap(),

                0xA0 => call!(self, LDY, 2, Imm).unwrap(),
                0xA4 => call!(self, LDY, 3, Zpg).unwrap(),
                0xB4 => call!(self, LDY, 4, ZpgX).unwrap(),
                0xAC => call!(self, LDY, 4, Abs).unwrap(),
                0xBC => call!(self, LDY, 4, AbsX).unwrap(),

                0x4A => call!(self, LSR, 2, Acc).unwrap(),
                0x46 => call!(self, LSR, 5, Zpg).unwrap(),
                0x56 => call!(self, LSR, 6, ZpgX).unwrap(),
                0x4E => call!(self, LSR, 6, Abs).unwrap(),
                0x5E => call!(self, LSR, 7, AbsX).unwrap(),

                0xEA => call!(self, NOP, 2, Impl).unwrap(),

                0x09 => call!(self, ORA, 2, Imm).unwrap(),
                0x05 => call!(self, ORA, 3, Zpg).unwrap(),
                0x15 => call!(self, ORA, 4, ZpgX).unwrap(),
                0x0D => call!(self, ORA, 4, Abs).unwrap(),
                0x1D => call!(self, ORA, 4, AbsX).unwrap(),
                0x19 => call!(self, ORA, 4, AbsY).unwrap(),
                0x01 => call!(self, ORA, 6, IndX).unwrap(),
                0x11 => call!(self, ORA, 5, IndY).unwrap(),

                0x48 => call!(self, PHA, 3, Impl).unwrap(),

                0x08 => call!(self, PHP, 3, Impl).unwrap(),

                0x68 => call!(self, PLA, 4, Impl).unwrap(),

                0x28 => call!(self, PLP, 4, Impl).unwrap(),

                0x2A => call!(self, ROL, 2, Acc).unwrap(),
                0x26 => call!(self, ROL, 5, Zpg).unwrap(),
                0x36 => call!(self, ROL, 6, ZpgX).unwrap(),
                0x2E => call!(self, ROL, 6, Abs).unwrap(),
                0x3E => call!(self, ROL, 7, AbsX).unwrap(),

                0x6A => call!(self, ROR, 2, Acc).unwrap(),
                0x66 => call!(self, ROR, 5, Zpg).unwrap(),
                0x76 => call!(self, ROR, 6, ZpgX).unwrap(),
                0x6E => call!(self, ROR, 6, Abs).unwrap(),
                0x7E => call!(self, ROR, 7, AbsX).unwrap(),

                0x40 => call!(self, RTI, 6, Impl).unwrap(),

                0x60 => call!(self, RTS, 6, Impl).unwrap(),

                0xE9 => call!(self, SBC, 2, Imm).unwrap(),
                0xE5 => call!(self, SBC, 3, Zpg).unwrap(),
                0xF5 => call!(self, SBC, 4, ZpgX).unwrap(),
                0xED => call!(self, SBC, 4, Abs).unwrap(),
                0xFD => call!(self, SBC, 4, AbsX).unwrap(),
                0xF9 => call!(self, SBC, 4, AbsY).unwrap(),
                0xE1 => call!(self, SBC, 6, IndX).unwrap(),
                0xF1 => call!(self, SBC, 5, IndY).unwrap(),

                0x38 => call!(self, SEC, 2, Impl).unwrap(),

                0xF8 => call!(self, SED, 2, Impl).unwrap(),

                0x78 => call!(self, SEI, 2, Impl).unwrap(),

                0x85 => call!(self, STA, 3, Zpg).unwrap(),
                0x95 => call!(self, STA, 4, ZpgX).unwrap(),
                0x8D => call!(self, STA, 4, Abs).unwrap(),
                0x9D => call!(self, STA, 5, AbsX).unwrap(),
                0x99 => call!(self, STA, 5, AbsY).unwrap(),
                0x81 => call!(self, STA, 6, IndX).unwrap(),
                0x91 => call!(self, STA, 6, IndY).unwrap(),

                0x86 => call!(self, STX, 3, Zpg).unwrap(),
                0x96 => call!(self, STX, 4, ZpgY).unwrap(),
                0x8E => call!(self, STX, 4, Abs).unwrap(),

                0x84 => call!(self, STY, 3, Zpg).unwrap(),
                0x94 => call!(self, STY, 4, ZpgX).unwrap(),
                0x8C => call!(self, STY, 4, Abs).unwrap(),

                0xAA => call!(self, TAX, 2, Impl).unwrap(),

                0xA8 => call!(self, TAY, 2, Impl).unwrap(),

                0xBA => call!(self, TSX, 2, Impl).unwrap(),

                0x8A => call!(self, TXA, 2, Impl).unwrap(),

                0x9A => call!(self, TXS, 2, Impl).unwrap(),

                0x98 => call!(self, TYA, 2, Impl).unwrap(),

                _ => panic!("Unkown Opcode: {:?}", instr)
            } as u32;
        }
        cur_cycles
    }
}