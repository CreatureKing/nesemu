use std::io;
use cpu::CPU;
use ppu::PPU;
use memory::Memory;

pub enum Action {
    STEP(u32),
    MEM(u16, Option<u16>),
    REG(Option<String>),
    STACK
}

use self::Action::*;

pub struct Debugger {
    cpu: CPU,
    ppu: PPU,
    memory: Memory,
    cpu_cycles: u32,
    ppu_cycles: u32
}

impl Debugger {
    pub fn new(cpu: CPU, ppu: PPU, memory: Memory) -> Debugger {
        Debugger {
            cpu,
            ppu,
            memory,
            cpu_cycles: 0,
            ppu_cycles: 0
        }
    }

    fn parse_input(&self, mut input: String) -> Result<Action, String> {
        // Get rid of newline
        input.pop();
        let mut split = input.split_whitespace();
        match split.next() {
            Some("s") | Some("step") => {
                if let Some(x) = split.next() {
                    let arg = u32::from_str_radix(x, 10).map_err(|e| e.to_string())?;
                    Ok(STEP(arg))
                } else {
                    Ok(STEP(1))
                }
            },
            Some("m") | Some("mem") | Some("memory") => {
                let mut args = vec![];
                for _ in 0..2 {
                    if let Some(x) = split.next() {
                        let arg = u16::from_str_radix(x, 16).map_err(|e| e.to_string())?;
                        args.push(Some(arg));
                    } else {
                        args.push(None);
                    }
                }
                Ok(MEM(args[0].ok_or_else(|| "Memory requires an argument!".to_owned())?, args[1]))
            },
            Some("r") | Some("reg") | Some("register") => {
                if let Some(x) = split.next() {
                    match x {
                        "a" | "x" | "y" | "pc" | "s" | "p" => Ok(REG(Some(x.to_owned()))),
                        _ => Err("Invalid Register argument!".to_owned())
                    }
                } else {
                    Ok(REG(None))
                }
            },
            Some("y") => {
                Ok(STACK)
            }
            Some(_) | None => Err("Invalid Command!".to_owned())
        }
    }

    fn handle_step(&mut self, cycles: u32) {
        for _ in 0..cycles {
            self.cpu_cycles += self.cpu.run(1);
            if self.cpu_cycles > self.ppu_cycles {
                let ppu_cycles_to_run = (self.cpu_cycles - (self.ppu_cycles / 3)) * 3;
                self.ppu_cycles += self.ppu.run(ppu_cycles_to_run);
            }
        }
    }

    fn handle_reg(&self, reg: Option<String>) {
//        debug!("PC is {:X}", self.reg.pc);
        let reg_string = match reg.as_ref().map(String::as_ref) {
            Some("a") => format!("{:02X}", self.cpu.a),
            Some("x") => format!("{:02X}", self.cpu.x),
            Some("y") => format!("{:02X}", self.cpu.y),
            Some("pc") => format!("{:02X}", self.cpu.pc),
            Some("s") => format!("{:02X}", self.cpu.s),
            Some("p") => {
                vec![
                    format!("Negative  = {:?}", self.cpu.status_negative()),
                    format!("Overflow  = {:?}", self.cpu.status_overflow()),
                    format!("Break     = {:?}", self.cpu.status_break()),
                    format!("Decimal   = {:?}", self.cpu.status_decimal()),
                    format!("Interrupt = {:?}", self.cpu.status_interrupt()),
                    format!("Zero      = {:?}", self.cpu.status_zero()),
                    format!("Carry     = {:?}", self.cpu.status_carry()),
                ].join("\n")
            }
            Some(_) => panic!("Should never happen"),
            None => {
                vec![
                    format!("Reg a  = {:02X}", self.cpu.a),
                    format!("Reg x  = {:02X}", self.cpu.x),
                    format!("Reg y  = {:02X}", self.cpu.y),
                    format!("Reg pc = {:02X}", self.cpu.pc),
                    format!("Reg p  = {:02X}", self.cpu.p),
                    format!("Reg s  = {:02X}", self.cpu.s)
                ].join("\n")
            }
        };
        println!("{}", reg_string);
    }

    fn handle_mem(&self, from: u16, to: Option<u16>) {
        let to_real = to.unwrap_or(from);
        for x in from..(to_real+1) {
            println!("[{:04X}] {:02X}", x, self.cpu.read(x as u16));
        }
    }

    fn handle_stack(&self) {
        if 0xFF - self.cpu.s == 0 {
            println!("Stack is Empty");
        }
        for x in ((0x0100 | self.cpu.s as u16) + 1)..0x0200u16 {
            println!("[{:04X}] {:02X}", x, self.cpu.read(x as u16));
        }
    }

    pub fn run(&mut self) {
        loop {
            let mut input = String::new();
            if io::stdin().read_line(&mut input).is_ok() {
                match self.parse_input(input) {
                    Ok(token) => {
                        match token {
                            STEP(x) => self.handle_step(x),
                            REG(x) => self.handle_reg(x),
                            MEM(x, y) => self.handle_mem(x, y),
                            STACK => self.handle_stack()
                        }
                    },
                    Err(error) => println!("Error: {}", error)
                }
            } else {
                panic!("Fatal IO Error!");
            }
        }
    }
}