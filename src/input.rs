extern crate sdl2;

use self::sdl2::event::{Event, EventPump};

pub enum ControllerEvent {
    Left,
    Right,
    Up,
    Down,
    A,
    B,
    Select,
    Start
}

pub struct Events {
    pump: EventPump
}

impl Events {
    pub fn pump(&mut self) {

    }
}