#![allow(unused_variables)]
#![allow(non_snake_case)]
#![allow(dead_code)]
#![allow(unknown_lints)]
#![allow(cyclomatic_complexity)]

#[macro_use] extern crate log;
extern crate env_logger;
extern crate byteorder;
//use byteorder::{BigEndian};
mod rom;
mod cpu;
mod ppu;
mod utils;
mod debugger;
mod memory;

use std::env;
use log::{LogRecord, LogLevelFilter};
use env_logger::LogBuilder;
use std::cell::RefCell;
use std::rc::Rc;

use rom::Rom;
use cpu::CPU;
use ppu::PPU;
use debugger::Debugger;

pub use memory::handlers::*;
pub use memory::Memory;

fn main() {
	let format = |record: &LogRecord| {
		format!("{}", record.args())
	};
	
	let mut builder = LogBuilder::new();
	builder.format(format).filter(None, LogLevelFilter::Info);
	
	if env::var("RUST_LOG").is_ok() {
		builder.parse(&env::var("RUST_LOG").unwrap());
	}
 
	builder.init().unwrap();
//     let rom = Rom::open("/home/jules/scripts/rust/nesemu/roms/dk.nes").unwrap();
//     let mapper = mappers::get_mapper(rom);
//     let cpu = CPU::new(mapper);
//     let mut debugger = Debugger::new(cpu);
//     debugger.run();

	let rom = Rom::open("/home/jules/dev/rust/nesemu/roms/nestest.nes").unwrap();

	let io_handler = Rc::new(RefCell::new(
		IoHandler::new()
	));
	let mapper_handler = Rc::new(RefCell::new(
		MapperHandler::new(memory::mappers::get_memory_mapper(rom))
	));
	let ram_handler = Rc::new(RefCell::new(
		RamHandler::new()
	));
	let vram_handler = Rc::new(RefCell::new(
		VRamHandler::new()
	));
	let ppu_reg_handler = Rc::new(RefCell::new(
		PPURegHandler::new(vram_handler.clone())
	));

	let memory = Memory::new(
		ram_handler.clone(),
		ppu_reg_handler.clone(),
		io_handler.clone(),
		mapper_handler.clone(),
		vram_handler.clone()
	);


	let mut cpu = CPU::new(memory.clone());
	let mut ppu = PPU::new(memory.clone(), ppu_reg_handler.clone(), vram_handler.clone());
	cpu.pc = 0xC000;
	cpu.s = 0xFD;
//	cpu.run(7000);
	let mut debugger = Debugger::new(cpu, ppu, memory.clone());
	debugger.run();
	/*
	let start = PreciseTime::now();
	cpu.run(4000);
	let end = PreciseTime::now();
	println!("{} microseconds.", start.to(end).num_microseconds().unwrap());
	*/
}
