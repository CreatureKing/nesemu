use memory::ReadableWritable;

pub struct IoHandler {}

impl ReadableWritable for IoHandler {
    fn read(&self, address: u16) -> u8 {
        panic!("Not implemented");
    }

    fn write(&mut self, address: u16, data: u8) {
        panic!("Not implemented");
    }

    fn reset(&mut self) {
        // NOP
    }
}

impl IoHandler {
    pub fn new() -> Self {
        IoHandler {}
    }
}