use memory::ReadableWritable;
use memory::Mapper;

pub struct MapperHandler {
    mapper: Box<Mapper>
}

impl ReadableWritable for MapperHandler {
    fn read(&self, address: u16) -> u8 {
        self.mapper.read(address)
    }

    fn write(&mut self, address: u16, data: u8) {
        self.mapper.write(address, data);
    }

    fn reset(&mut self) {
        // NOP
    }
}

impl MapperHandler {
    pub fn new(mapper: Box<Mapper>) -> Self {
        MapperHandler {
            mapper
        }
    }
}