use memory::handlers::vram::VRamHandler;
use std::cell::RefCell;
use std::rc::Rc;
use memory::ReadableWritable;
use utils;

pub struct PPURegHandler {
    vram_handler: Rc<RefCell<VRamHandler>>,
    address_latch: u16, // 15 Bit address latch used by ppu_scroll and ppu_addr
    oam: Vec<u8>,
    ppu_ctrl: u8,
    ppu_mask: u8,
    ppu_status: u8,
    oam_addr: u8
}

impl PPURegHandler {
    pub fn new(vram_handler: Rc<RefCell<VRamHandler>>) -> Self {
        let mut ppu_reg_handler = PPURegHandler {
            vram_handler,
            address_latch: 0, // 15 Bit address latch used by ppu_scroll and ppu_addr
            oam: vec![0; 256],
            ppu_ctrl: 0,
            ppu_mask: 0,
            ppu_status: 0,
            oam_addr: 0
        };
        ppu_reg_handler.reset();
        ppu_reg_handler
    }

    pub fn read(&mut self, address: u16) -> u8 {
        match address {
            0x2000 => self.ppu_ctrl,
            0x2001 => self.ppu_mask,
            0x2002 => {
                self.address_latch = 0; // Reset address latch?
                self.ppu_status
            },
            0x2003 => self.oam_addr,
            0x2004 => {
                self.oam[self.oam_addr as usize]
            },
            0x2007 => self.read_vram(),
			_ => panic!(format!("PPU Reg read out of bounds: {:02X}", address))
        }
    }

    pub fn write(&mut self, address: u16, data: u8) {
        match address {
            0x2000 => self.ppu_ctrl = data,
            0x2001 => self.ppu_mask = data,
            0x2002 => self.ppu_status = data,
            0x2003 => self.oam_addr = data,
            0x2004 => {
                self.oam[self.oam_addr as usize] = data;
            },
            0x2005...0x2006 => {
                // ppu_scroll and ppu_addr
                self.address_latch <<= 8;
                self.address_latch = (self.address_latch & 0xFF00) | data as u16;
            },
            0x2007 => self.write_vram(data),
			_ => panic!(format!("PPU Reg write out of bounds: {:02X}", address))
        };
    }

    pub fn reset(&mut self) {
        // NOP
    }

    pub fn write_vram(&mut self, data: u8) {
        self.vram_handler.borrow_mut().write(self.address_latch, data);
        self.address_latch += self.vram_address_increment();
    }

    pub fn read_vram(&mut self) -> u8 {
        let data = self.vram_handler.borrow_mut().read(self.address_latch);
        self.address_latch += self.vram_address_increment();
        data
    }

    pub fn write_oam_dma(&mut self, data: &[u8]) {
        self.oam.clone_from_slice(data);
    }

    pub fn base_nametable_address(&self) -> u16 {
        match self.ppu_ctrl & 0x3 {
            0x0 => 0x2000,
            0x1 => 0x2400,
            0x2 => 0x2800,
            0x3 => 0x2C00,
            _ => panic!{"Should never happen"}
        }
    }

    pub fn vram_address_increment(&self) -> u16 {
        if self.ppu_ctrl & 0x4 > 0 {
            0x32
        } else {
            0x1
        }
    }

    pub fn sprite_pattern_table_address(&self) -> u16 {
        if self.ppu_ctrl & 0x8 > 0 {
            0x1000
        } else {
            0x0
        }
    }

    pub fn background_pattern_table_address(&self) -> u16 {
        if self.ppu_ctrl & 0x10 > 0 {
            0x1000
        } else {
            0x0
        }
    }

    pub fn sprite_size(&self) -> (u8, u8) {
        if self.ppu_ctrl & 0x20 > 0 {
            (8, 16)
        } else {
            (8, 8)
        }
    }
    
    pub fn generate_nmi_at_next_vblank(&self) -> bool {
        self.ppu_ctrl & 0x80 > 0
    }

    pub fn scrollx_offset(&self) -> u16 {
         if (self.ppu_ctrl & 0x1) > 0 { 256 } else { 0 }
    }

    pub fn scrolly_offset(&self) -> u16 {
         if (self.ppu_ctrl & 0x2) > 0 { 240 } else { 0 }
    }

    pub fn grayscale(&self) -> bool {
        self.ppu_mask & 0x1 > 0
    }

    pub fn show_background_leftmost(&self) -> bool {
        self.ppu_mask & 0x2 > 0
    }

    fn show_sprites_leftmost(&self) -> bool {
        self.ppu_mask & 0x4 > 0
    }

    fn show_background(&self) ->  bool {
        self.ppu_mask & 0x8 > 0
    }

    fn show_sprites(&self) -> bool {
        self.ppu_mask & 0x10 > 0
    }

    fn emphasize_red(&self) -> bool {
        self.ppu_mask & 0x20 > 0
    }

    fn emphasize_green(&self) -> bool {
        self.ppu_mask & 0x40 > 0
    }

    fn emphasize_blue(&self) -> bool {
        self.ppu_mask & 0x40 > 0
    }

    fn sprite_overflow(&self) -> bool {
        self.ppu_status & 0x20 > 0
    }

    fn set_sprite_overflow(&mut self, status: bool) {
        self.ppu_status = utils::set_bit(self.ppu_status, 5, status);
    }

    fn sprite0_hit(&self) -> bool {
        self.ppu_status & 0x40 > 0
    }

    fn set_sprite0_hit(&mut self, status: bool) {
        self.ppu_status = utils::set_bit(self.ppu_status, 6, status);
    }

    fn vblank(&self) -> bool {
        self.ppu_status & 0x80 > 0
    }

    fn set_vblank(&mut self, status: bool) {
        self.ppu_status = utils::set_bit(self.ppu_status, 7, status);
    }
}