use memory::ReadableWritable;

pub struct RamHandler {
	mem: Vec<u8>
}

impl ReadableWritable for RamHandler {
	fn read(&self, address: u16) -> u8 {
		match address {
			0x0000...0x07FF => self.mem[address as usize],
			0x0800...0x1FFF => self.mem[(address & 0x07FF) as usize],
			_ => panic!(format!("Ram read out of bounds: {:02X}", address))
		}
	}

	fn write(&mut self, address: u16, data: u8) {
		self.mem[address as usize] = data;
	}

    fn reset(&mut self) {
		for i in 0usize..0x0800usize {
			self.mem[i] = 0;
		}
    }
}

impl RamHandler {
	pub fn new() -> Self {
		RamHandler {
			mem: vec![0; 0x0800]
		}
	}
}