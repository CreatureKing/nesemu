use memory::ReadableWritable;

pub struct VRamHandler {
    mem: Vec<u8> 
}

impl ReadableWritable for VRamHandler {
    fn read(&self, address: u16) -> u8 {
		match address {
			0x0000...0x2FFF => self.mem[address as usize],
			0x3000...0x3EFF => self.mem[(address - 0x1000) as usize],
			0x3F00...0x3F1F => self.mem[address as usize],
			0x3F20...0x3FFF => self.mem[(0x3F00 + (address - 0x3F20) % 0x0020) as usize],
			_ => panic!(format!("VRam read out of bounds: {:02X}", address))
		}
    }

    fn write(&mut self, address: u16, data: u8) {
        self.mem[address as usize] = data;
    }

    fn reset(&mut self) {
		for i in 0usize..0x4000usize {
			self.mem[i] = 0;
		}
    }
}

impl VRamHandler {
    pub fn new() -> Self {
        VRamHandler {
			mem: vec![0; 0x4000],
        }
    }
}