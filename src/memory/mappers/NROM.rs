use memory::Mapper;
use rom::Rom;

pub struct NROM {
    rom: Rom
}

impl Mapper for NROM {
    fn read(&self, address: u16) -> u8 {
        match address {
            0x8000...0xBFFF => self.rom.pgr_rom[(address - 0x8000) as usize],
            0xC000...0xFFFF => self.rom.pgr_rom[(address - 0xC000) as usize],
            _ => panic!(format!("NROM read out of bounds: {:02X}", address))
        }
    }

    fn write(&mut self, address: u16, data: u8) {
        panic!("NROM has no write addressible memory");
    }

    fn reset(&mut self) {
        // NOP
    }
}

impl NROM {
    pub fn new(rom: Rom) -> Self {
        NROM {
            rom
        }
    }
}