pub mod NROM;

use memory::Mapper;
use rom::Rom;

pub fn get_memory_mapper(rom: Rom) -> Box<Mapper> {
	let mapper_num = rom.mapper_number;
	match mapper_num {
		0 => {
			Box::new(NROM::NROM::new(rom))
		},
		_ => panic!("Mapper {} not implemented", mapper_num)
	}
}