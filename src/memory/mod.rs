pub trait ReadableWritable {
	fn read(&self, address: u16) -> u8;
	fn write(&mut self, address: u16, data: u8);
	fn reset(&mut self);
}

pub trait Mapper {
	fn read(&self, address: u16) -> u8;
	fn write(&mut self, address: u16, data: u8);
	fn reset(&mut self);
}

pub mod handlers;
pub mod mappers;

use std::cell::RefCell;
use std::rc::Rc;
use self::handlers::*;

pub struct Memory {
	ram_handler: Rc<RefCell<RamHandler>>,
	ppu_reg_handler: Rc<RefCell<PPURegHandler>>,
	io_handler: Rc<RefCell<IoHandler>>,
	mapper_handler: Rc<RefCell<MapperHandler>>,
	vram_handler: Rc<RefCell<VRamHandler>>
}

impl Clone for Memory {
	fn clone(&self) -> Self {
		Memory {
			ram_handler: self.ram_handler.clone(),
			ppu_reg_handler: self.ppu_reg_handler.clone(),
			io_handler: self.io_handler.clone(),
			mapper_handler: self.mapper_handler.clone(),
			vram_handler: self.vram_handler.clone()
		}
	} 
}

impl Memory {
	pub fn new(
		ram_handler: Rc<RefCell<RamHandler>>,
		ppu_reg_handler: Rc<RefCell<PPURegHandler>>,
		io_handler: Rc<RefCell<IoHandler>>,
		mapper_handler: Rc<RefCell<MapperHandler>>,
		vram_handler: Rc<RefCell<VRamHandler>>
	) -> Self {
		Memory {
			ram_handler,
			ppu_reg_handler,
			io_handler,
			mapper_handler,
			vram_handler
		}
	}

	// CPU Address Space
	pub fn cpu_read(&self, address: u16) -> u8 {
		match address {
			0x0000...0x1FFF => self.ram_handler.borrow().read(address),
			0x2000...0x3FFF => self.ppu_reg_handler.borrow().read(address),
			0x4000...0x401F => self.io_handler.borrow().read(address), 
			_ => self.mapper_handler.borrow().read(address)
		}
	}

	pub fn cpu_write(&mut self, address: u16, data: u8) {
		match address {
			0x0000...0x1FFF => self.ram_handler.borrow_mut().write(address, data),
			0x2000...0x3FFF => self.ppu_reg_handler.borrow_mut().write(address, data),
			0x4000...0x401F => self.io_handler.borrow_mut().write(address, data), 
			0x4014 => {
				let start = (data as u16) << 8;
				let end = (((data as u16) << 8) | 0x00FF) + 1;
				let load_data: Vec<u8> = (start..end)
					.into_iter()
					.map(|addr| self.cpu_read(addr))
					.collect();
				self.ppu_reg_handler.borrow_mut().write_oam_dma(&load_data);
			},
			_ => self.mapper_handler.borrow_mut().write(address, data)
		};
	}

	pub fn vram_read(&self, address: u16) -> u8 {
		self.vram_handler.borrow().read(address)
	}

	pub fn vram_write(&mut self, address: u16, data: u8) {
		self.vram_handler.borrow_mut().write(address, data);
	}

	pub fn cpu_reset(&mut self) {
		self.ram_handler.borrow_mut().reset();
		self.ppu_reg_handler.borrow_mut().reset();
		self.io_handler.borrow_mut().reset();
		self.mapper_handler.borrow_mut().reset();
	}

	pub fn vram_reset(&mut self) {
		self.vram_handler.borrow_mut().reset();
	}
}