use memory::handlers::PPURegHandler;
use memory::handlers::VRamHandler;
use std::cell::RefCell;
use std::rc::Rc;
use memory::Memory;

pub struct PPU {
	frame_cycle: u32,
	memory: Memory,
	ppu_reg_handler: Rc<RefCell<PPURegHandler>>,
	vram_handler: Rc<RefCell<VRamHandler>>,
	nametable_byte: u8,
	attribute_table_byte: u8,
	tile_bitmap_low_byte: u8,
	tile_bitmap_high_byte: u8,
	high_byte_addr: u16 // Temporary value because vram addr is going to be wrong for high tile byte after read
}

impl PPU {
	pub fn new(
		memory: Memory,
		ppu_reg_handler: Rc<RefCell<PPURegHandler>>,
		vram_handler: Rc<RefCell<VRamHandler>>,
	) -> Self {
		PPU {
			frame_cycle: 0,
			ppu_reg_handler,
			memory,
			vram_handler,
			nametable_byte: 0,
			attribute_table_byte: 0,
			tile_bitmap_low_byte: 0,
			tile_bitmap_high_byte: 0,
			high_byte_addr: 0
		}
	}

	pub fn run(&mut self, cycles: u32) -> u32 {
		let mut cur_cycles: u32 = 0;
		while cur_cycles < cycles {
			let cycles_taken = match self.frame_cycle {
				0 => 1,
				1...256 => {
					let norm = (self.frame_cycle - 1) % 8;
					match norm {
						0...1 => self.nametable_byte = 0,
						2...3 => self.attribute_table_byte = 0,
						4...5 => self.nametable_byte = 0,
						6...8 => self.nametable_byte = 0,
						_ => panic!("Should never happen")
					};
					2 // Fetch always takes 2 cycles
				}
				257...320 => 1,
				321...336 => 1,
			    _ => 1
			};
			cur_cycles += cycles_taken;
			self.frame_cycle = (self.frame_cycle + cycles_taken) % 341;
		}
		cur_cycles
	}
}
