use std;
use std::fs::File;
use std::io::Read;

#[derive(Debug)]
pub struct Flags {
    pub mirroring_vertical: bool,
    pub battery_backed_pgr_rom: bool,
    pub trainer: bool,
    pub ignore_mirroring: bool,
    pub vs_unisystem: bool,
    pub player_choice_10: bool,
    pub pal: bool
}

#[derive(Debug)]
pub struct Rom {
    pub pgr_rom: Vec<u8>,
    pub chr_rom: Vec<u8>,
    pub player_choice_inst_rom: Vec<u8>,
    pub player_choice_prom: Vec<u8>,
    pub size_of_pgr_ram: u8,
    pub mapper_number: u8,
    pub flags: Flags
}

#[derive(Debug)]
pub enum RomError {
    LoadError(String)
}

pub type RomResult<T> = Result<T, RomError>;

impl From<std::io::Error> for RomError {
    fn from(err: std::io::Error) -> RomError {
        RomError::LoadError(format!("Failed to read ROM. I/O Error. {:?}", err))
    }
}

impl Rom {
    /*
    pub fn read_bytes(file: &mut File, count: usize) -> Result<Vec<u8>, std::io::Error> {
        let mut buf = vec![0; count];
        file.read_exact(&mut buf)?;
        Ok(buf)
    }
    */

    pub fn read_bytes(file: &mut File, count: usize) -> Result<Vec<u8>, std::io::Error> {
        let mut buf = vec![0; count];
        let mut read = 0;
        while read < count {
            let bytes_read = file.read(&mut buf[read..count])?;
            if bytes_read == 0 { break; }
            read += bytes_read;
        }
        Ok(buf)
    }

    pub fn open(path: &str) -> RomResult<Rom> {
        let mut file = File::open(path)?;
        let header = Rom::read_bytes(&mut file, 16_usize)?;
        let constant = &header[..4];
        if constant != [0x4E, 0x45, 0x53, 0x1A] {
            return Err(RomError::LoadError("Did not have NES header bytes. Is this a valid ROM?".to_string()));
        }

        let size_of_pgr_rom = header[4];
        let size_of_chr_rom = header[5];

        let flag6 = header[6];
        let mirroring_vertical = (flag6 & 0b00000001) > 0;
        let battery_backed_pgr_rom = (flag6 & 0b00000010) > 0;
        let trainer = (flag6 & 0b00000100) > 0;
        let ignore_mirroring = (flag6 & 0b00001000) > 0;
        let lower_mapper_nibble = (flag6 & 0b11110000) >> 4;

        let flag7 = header[7];
        let vs_unisystem = (flag7 & 0b00000001) > 0;
        let player_choice_10 = (flag7 & 0b00000010) > 0;
        let upper_mapper_nibble = flag7 & 0b11110000;
        // initial support is not NES 2.0
        // let nes2_format = (flag7 & 0b00001100) == 2;

        let size_of_pgr_ram = header[8];

        let flag9 = header[9];
        let pal = (flag9 & 0b00000001) > 0;
        if flag9 & 0b11111110 != 0 {
            return Err(
                RomError::LoadError("Invalid extra bits (0-7) in flag9 of the header. Should be 0's.".to_string())
            )
        }

        // This emulator does not respect flag 10
        // let flag10 = header[10];

        let rest = &header[11..];
        if rest != [0, 0, 0, 0, 0] {
            return Err(
                RomError::LoadError("Invalid extra bytes (11-15) in the header. Should be 0's.".to_string())
            )
        }

        let pgr_rom: Vec<u8> = Rom::read_bytes(&mut file, 16384 * size_of_pgr_rom as usize)?;
        let chr_rom: Vec<u8> = Rom::read_bytes(&mut file, 8192 * size_of_chr_rom as usize)?;
        let mut inst_rom: Vec<u8> = vec![];
        let prom = if player_choice_10 {
            inst_rom = Rom::read_bytes(&mut file, 8192)?;
            Rom::read_bytes(&mut file, 32)?
        } else { vec![] };

        Ok(Rom {
            pgr_rom: pgr_rom,
            chr_rom: chr_rom,
            player_choice_inst_rom: inst_rom,
            player_choice_prom: prom,
            size_of_pgr_ram: size_of_pgr_ram,
            mapper_number: upper_mapper_nibble | lower_mapper_nibble,
            flags: Flags {
                mirroring_vertical: mirroring_vertical,
                battery_backed_pgr_rom: battery_backed_pgr_rom,
                trainer: trainer,
                ignore_mirroring: ignore_mirroring,
                vs_unisystem: vs_unisystem,
                player_choice_10: player_choice_10,
                pal: pal
            }
        })
    }
}
