use cpu::AddressMode;
use cpu::CPU;

use cpu::AddressMode::*;

fn little_end(op1: u8, op2: u8) -> u16 {
    ((op2 as u16) << 8 | op1 as u16)
}

pub fn format_op_string(cpu: &CPU, opcode: u8, inst: &str, mode: &AddressMode, 
                        operand1: &Option<u8>, operand2: &Option<u8>) -> String {
    
    let op1_str = match *operand1 {
        Some(op1) => format!("{:02X}", op1),
        None => "  ".to_owned()
    };
    let op2_str = match *operand2 {
        Some(op2) => format!("{:02X}", op2),
        None => "  ".to_owned()
    };
    let pc_offset = 1 + operand1.map_or(0, |x| 1) + operand2.map_or(0, |x| 1);
    let pc_str = format!("{:04X}", cpu.pc - pc_offset);

    let addr_str = format!("{:28}", match *mode {
        Imm => format!("#${:02X}", operand1.unwrap()),
        Abs => format!("${:04X}", little_end(operand1.unwrap(), operand2.unwrap())),
        Zpg => format!("${:02X}", operand1.unwrap()),
        Rel => format!("${:02X}", {
            let op1 = operand1.unwrap() as u16;
            if op1 & 0x80 > 0 {
                cpu.pc - (0x0100 - op1) 
            } else {
                cpu.pc + op1
            }
        }),
        AbsX => format!("${:04X},X", little_end(operand1.unwrap(), operand2.unwrap())),
        AbsY => format!("${:04X},Y", little_end(operand1.unwrap(), operand2.unwrap())),
        ZpgX => format!("${:02X},X", operand1.unwrap()),
        ZpgY => format!("${:02X},Y", operand1.unwrap()),
        Ind => format!("(${:04X})", little_end(operand1.unwrap(), operand2.unwrap())),
        IndX => format!("(${:02X},X)", operand1.unwrap()),
        IndY => format!("(${:02X}),Y", operand1.unwrap()),
        _ => String::new()
    });
    format!("{}  {:02X} {} {}  {} {}A:{:02X} X:{:02X} Y:{:02X} P:{:02X} SP:{:02X}",
        pc_str,
        opcode,
        op1_str,
        op2_str,
        inst,
        addr_str,
        cpu.a,
        cpu.x,
        cpu.y,
        cpu.p,
        cpu.s
    )
}

pub fn set_bit(number: u8, bit: u8, to: bool) -> u8 {
    if to {
        number | (1 << bit)
    } else {
        number & !(1 << bit)
    }
}